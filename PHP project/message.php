<?php session_start();
include_once 'includes/dbh.inc.php';
    if(!isset($_SESSION['username'])){
        header('location:loginform.php');
    }
?>
<html>
	<head>
    <title>message</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                setInterval(function(){
                    $('#render').load("listMessages.php");
                    refresh();
                },5000);
            });
        </script>
    </head>
	<body>
  <?php
    include ('includes/navbar.php');
    
    ?>
        
		<h1>Pošaljite poruku:</h1>

        <?php 
        $theOtherGuy = $_GET['name'];
        $_SESSION['razgovaramSa']=$theOtherGuy;
        if(isset ($_SESSION['error'])){
            $error = $_SESSION["error"];
                        echo "<span>$error</span>";
            
        }
        ?>
		 <form action='messageSystem.php' method='POST'>
		 <table>
                <input id="to" type="hidden" name="to" value ="<?=$_GET["name"]?>">
                <input id="from" type="hidden" name="from" value ="<?=$_SESSION['username']?>">
				 <tr>
					<td>Message: </td><td><input id="mess" type='text' name='message' /></td>
				</tr>
				 <tr>
					<td></td><td><input id="mess-submit" type='submit' value='Pošalji' name='sendMessage' /></td>
				</tr>

		 </table>
        <div id="render"></div>
         </form>;

	</body>
</html>

<?php
    unset($_SESSION["error"]);
?>