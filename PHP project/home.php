<?php

session_start();
if(!isset($_SESSION['username'])){
    header('location:index.php');
}

if($_SESSION['role']=='user'){
    header("Location:oglasi.php");
}

?>

<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        aside {
        padding-left: 15px;
        margin-left: 15px;
        float: right;
        font-style: italic;
        }
        </style>
</head>
<body>
    <?php
        include_once 'includes/dbh.inc.php'; 
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        include ('includes/navbar.php');
    ?>

    <div class="container-fluid" id="homepanels">
        <h1>Pozdrav <?php echo $_SESSION['username']; ?> </h1>
    </div>
    <div class="gallery-upload" id="homepanels">
        <h2>Dodajte sliku u galeriju</h2>
        <form action="includes/gallery-upload.inc.php" method ="POST" enctype="multipart/form-data">
            <div><input type="text" name="filename" placeholder="File name"></div>
            <div><input type="text" name="filetitle" placeholder="Image title"></div>
            <div><input type="text" name="filedesc" placeholder="Image description"></div>
            <div><input type="file" name="file"></div>
            <div><button type="submit" name="submit">Upload</button></div>
        </form>
    </div>

    <div class="container-fluid" id="homepanels">
        <h2>Dodajte novi oglas</h2>
        <form action="dodajOglas.php" method ="POST" enctype="multipart/form-data">
            <div><input type="text" name="imeOglasa" placeholder="Naziv oglasa"></div>
            <div>
            <input type="file" name="file">
            <label for="file">Dodajte fotografiju</label></div>
            
            <input type="hidden" name="imeOglasivaca" value ="<?=$_SESSION['username']?>">
            <div><textarea name="opisOglasa" rows="5" cols="49" placeholder="Opis oglasa..."></textarea></div>
            <div><button type="submit" name="add">Add</button></div>
        </form>
    </div>

    <aside>
    <h3>Moje poruke:</h1>
        <table>
            <tbody>
                <?php
                    $user = $_SESSION['username'];
                    $qu = mysqli_query($conn, "SELECT * FROM `messages` WHERE `messageto`='$user'");
                    if (mysqli_num_rows($qu) > 0) {
                        while ($row = mysqli_fetch_array($qu)) {
                            echo '<tr><td><b>'.$row["messagefrom"].'</b></td><td>'.$row["message"].'</td>';
                            echo '<td><a href="message.php?name=' . $row['messagefrom'] . '">Odgovorite</a></td></tr>';
                        }
                    }
                ?>
            </tbody>
        </table>
        </aside>
</body>
</html>