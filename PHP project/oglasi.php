<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Oglasi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
            #slika{
              
              width: 300px;
              height: 235px;
              background-position: center;
              background-repeat: no-repeat;
              background-size: cover;
              margin: 10px;
              border: 5px solid #555;
              }
      #jedanoglas{
          margin:50px;
          padding:50px;
          border:solid;
          border-color: #ffd252;
          background-color:white;
          flex-wrap: wrap;
        }
      body{
        background-image: url("bg.jpg");
      }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
<body>
  
<?php
include ('includes/navbar.php');
include_once 'includes/dbh.inc.php';
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM oglasi";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo'<div class=container style="display: flex;align-items: center;flex-wrap: wrap;">';
    while($row = $result->fetch_assoc()) {
    echo'<div id="jedanoglas">';
        
        echo '
                    <a href="#">
                    <div id="slika" style="background-image: url(img/oglasi/'.$row["imgFullName"].');"></div>
                    </a>';
                    
    echo'<div>';
    echo 'Oglašavač : <b>'.$row["imeOglasivaca"].'</b>';
    echo'</div>';echo'<div>';
    echo 'Naziv : <b>'.$row["imeOglasa"].'</b>';
    echo'</div>';echo'<div>';
    echo 'Opis : '.$row["opisOglasa"];
    echo'</div>';echo'<div>';
    echo '<button><a href="message.php?name=' . $row['imeOglasivaca'] . '">Pošaljite poruku</a></button>';
    echo'</div>';
    
    echo'</div>';
    }
    echo'</div>';
  }

  
$conn->close();

?>

</body>
</html>