<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Beebook</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="gallerystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
<body>

<?php
    include ('includes/navbar.php');
    ?>
    <div class="gallery-container" id="gall" >
        <?php
        include_once 'includes/dbh.inc.php';
        $sql= "SELECT * FROM gallery ORDER BY idGallery DESC;";
        $stmt =mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "SQL statement failed";
        }else{
            mysqli_stmt_execute($stmt);
            $result=mysqli_stmt_get_result($stmt);

            while($row=mysqli_fetch_assoc($result)){
                echo '
                <a href="#">
                <div id="my-image" style="background-image: url(img/gallery/'.$row["imgFullNameGallery"].');"></div>
                <h3>'.$row["titleGallery"].'</h3>
                <p>'.$row["descGallery"].'</p>
                </a>';
            }
        }
     ?>
    </div>
    <script>
$("my-image").toggle(function()
    {$(this).animate({width: "400px"}, 'slow');},
    function()
    {$(this).animate({width: "120px"}, 'slow');
});
    </script>
</body>
</html>
