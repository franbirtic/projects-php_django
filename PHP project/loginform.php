<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Beebook</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<?php
include ('includes/navbar.php');
?>
  
<div class="container">
  
  <div id="login">
        <?php 
        if(isset ($_SESSION['error'])){
            $error = $_SESSION["error"];
                        echo "<span>$error</span>";
            
        }
        ?>
    <form id ="form" autocomplete="off" action="validation.php" method="post">
        <h1>Log in</h1>
        <label for="username">Korisničko ime:</label>
        <div class="field">
            <input type="text" name="username" required 
                placeholder="Username" id="username"/>
        </div>
        <label for="password">Lozinka:</label>
        <div class="filed">
            <input type="password" name="password" required
                placeholder="Password" class="field" id="password"/>
        </div>
        <div class="field center">
            <input type="submit" value="Login"/>
        </div>
        <div class="filed center">
            Not registered?
            <button type="button" id="link_to_register">Go to register</button>
        </div>
        <input type="hidden" name="action" value="login">
    </form>
</div>


<div id="register">
    <form id ="form" autocomplete="off" action="registration.php" method="post">
        <h1>Sign up</h1>
        <div class="field">
            <label for="usernamesignup">Korisničko ime</label>
            <input type="text" name="usernamesignup" required
                placeholder="Korisničko ime" id="usernamesignup"/>
        </div>
        <div class="field">
            <label for="fname">Ime</label>
            <input type="text" name="fname" required placeholder="Vaše ime" id="fname">
        </div>
        <div class="field">
            <label for="lname">Prezime</label>
            <input type="text" name="lname" required placeholder="Vaše prezime" id="lname">
        </div>
        <div class="field">
            <label for="location">Lokacija</label>
            <input type="text" name="location" required placeholder="Lokacija" id="location">
        </div>
        <div class="field">
            <label for="email">E-mail addresa</label>
            <input type="email" name="email" required placeholder="e-mail" id="email">
        </div>
        <div class="field">
            <label for="passwordsignup">Lozinka</label>
            <input type="password" name="passwordsignup" required
                placeholder="Lozinka" id="passwordsignup"/>
        </div>
        <div class="field">
            <label for="password_confirm">Ponovite lozinku</label>
            <input type="password" name="password_confirm" required
                placeholder="Ponovite lozinku" id="password_confirm"/>
        </div>
            <div class="field center">
            <label for="role">Uloga: </label>
            <input type="radio" name="role" value="gost">gost
            <input type="radio" name="role" value="oglašavač">oglašavač
            </div>
        <div class="field center">
            <input type="submit" value="Registrirajte se"/>
        </div>
        <div class="field center">
            Već ste registrirani?
            <button type="button" id="link_to_login">Prijava</button>
        </div>
        <input type="hidden" name="action" value="register">
    </form>
</div>
</div>
<script> 
    $(document).ready(function(){

        $("#register").slideToggle(0);
      });
    </script>
<script>
    $(document).ready(function(){
        var ButtonValue;
        $("#link_to_register, #link_to_login").click(function(){
            $("#login").slideToggle(300);
            $("#register").slideToggle(300);
            $.post("set_msg.php", { message: "delete" }, function(result) {
                $("#divmsg").html(result);
            });
        });
        $("input:submit").click(function() {
            ButtonValue = $(this).val();
        });
        $("form").submit(function(e) {
            // var proceed = true;
            var eMsg        = "";
            var name        = "";
            var pwd         = "";
            var pwdconfirm  = "";
            var numDigits   = 0;
            var numSpecial  = 0;
            if(ButtonValue == "Login") {
                name        = $("#username").val();
                pwd         = $("#password").val();
            }
            else {
                name        = $("#usernamesignup").val();
                pwd         = $("#passwordsignup").val();
                pwdconfirm  = $("#password_confirm").val();
            }                    
            if(name.length < 4) {
                eMsg += "Korisničko ime mora sadržavati barem 4 znaka.\n";
                // proceed = false;
            }
            if(pwd.length < 6) {
                eMsg += "Lozinka mora sadržavati barem 6 znakova.\n";
                if(ButtonValue == "Registrirajte se") {
                    if(pwdconfirm.length < 6) {
                        eMsg += "Potvrdna lozinka mora sadržavati barem 6 znakova.\n";
                    }
                }
                // proceed = false;
            }
             {
                var i;
                if(ButtonValue == "Registrirajte se") {
                    if(pwdconfirm.length < 6) {
                        eMsg += "Lozinka mora sadržavati barem 6 znakova.\n";
                        // proceed = false;
                    }
                    if(pwd != pwdconfirm) {
                        eMsg += "Lozinke moraju biti jednake.\n";
                        // proceed = false;
                    }
                }
                for(i = 0; i < pwd.length; i++) {
                    if(pwd.charAt(i) >= '0' && pwd.charAt(i) <= '9') {
                        numDigits++;
                    }
                    
                    switch(pwd.charAt(i)) {
                        case '_': numSpecial++; break;
                        case '.': numSpecial++; break;
                        case ',': numSpecial++; break;
                        case '!': numSpecial++; break;
                        case '#': numSpecial++; break;
                        case '$': numSpecial++; break;
                        case '%': numSpecial++; break;
                        case '?': numSpecial++; break;
                    }
                }
                if(numDigits == 0) {
                    eMsg += "Lozinka mora sadržavati barem jednu znamenku.\n";
                    // proceed = false;
                }
                if(numSpecial == 0) {
                    eMsg += "Lozinka mora sadržavati barem jedan poseban znak (_ . , ! # $ % ?).\n";
                    // proceed = false;
                }
                
            }
            if(eMsg.length > 0) {
                e.preventDefault();
                alert("Pogreška prilikom unosa podataka:\n" + eMsg);
                return false;
            }
            else {
                // Not neccesary because form will send data if no errors detected
                // $("form").submit();
                return true;
            }
            // return proceed;
        });
    });
</script>
</body>
</html>
<?php
    unset($_SESSION["error"]);
?>