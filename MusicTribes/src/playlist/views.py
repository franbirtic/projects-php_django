from django.shortcuts import render, get_object_or_404,redirect
from .models import playlist, song, vote, comment
from .forms import CreatePlaylistForm, AddSongForm, CreateCommentForm
from Tribe.models import Tribe, Membership
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.

@login_required(login_url="/login")
def Like(request, id, play_id):
    TTribe=get_object_or_404(playlist, pk=play_id).tribe
    users=Membership.objects.filter(Tribe=TTribe).values('User')
    user_list=[]
    for i in users:
        user_list.append(i['User'])
    user_list.append(TTribe.chieftain.id)
    if request.user.id in user_list:    
        Song = get_object_or_404(song, pk=id)
        if vote.objects.filter(user=request.user, song=Song).exists():
            obj = get_object_or_404(vote, user=request.user, song=Song) 
            obj.delete()
            Song.num_likes=Song.num_likes-1
            Song.save()
        else:
            newLike=vote(song=Song, user= request.user)
            newLike.save()
            Song.num_likes=Song.num_likes+1
            Song.save()

        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))
    else:
        messages.info(request, ('You are not a member in this tribe!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))


@login_required(login_url="/login")
def send_comment(request, id, play_id):
    TTribe=get_object_or_404(playlist, pk=play_id).tribe
    users=Membership.objects.filter(Tribe=TTribe).values('User')
    user_list=[]
    for i in users:
        user_list.append(i['User'])
    user_list.append(TTribe.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            comment_form=CreateCommentForm(request.POST)
        
            if comment_form.is_valid():
                NewComment=comment_form.save(commit=False)
                NewComment.song = get_object_or_404(song, pk=id)
                NewComment.user=request.user
                NewComment.song.num_comments=NewComment.song.num_comments+1
                NewComment=comment_form.save()
                NewComment.song.save()
            else:
                messages.success(request, ('not valid form!'))
        else:
            messages.success(request, ('something else!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))
    else:
        messages.info(request, ('You are not a member in this tribe!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))

def playlist_detail (request,id):
    if request.method== 'POST':
        QueryDict = request.POST
        sort= QueryDict.get("sort")
    else:
        sort='-num_likes'
    if request.user.is_authenticated:
        tribe=get_object_or_404(playlist, pk=id).tribe
        comment_form=CreateCommentForm()
        Playlist=get_object_or_404(playlist, pk=id)
        likes=vote.objects.filter(user=request.user).values('song')
        title_list=song.objects.filter(Playlist=Playlist).order_by(sort)
        songs=[]
        for songy in title_list:
            songs.append(songy)
        all_comments=comment.objects.all()    
        comments=[]
        for comm in all_comments:
            for sng in songs:
                if comm.song==sng:
                    comments.append(comm)
        
        context ={
            'playlist':Playlist,
            'songs':songs,
            'likes':likes,
            'comments':comments,
            'comment_form':comment_form,
            'chieftain':tribe.chieftain
        }
        return render (request, 'playlist_details.html', context)
    else:
        Playlist=get_object_or_404(playlist, pk=id)
        title_list=song.objects.filter(Playlist=Playlist)
        songs=[]
        for songy in title_list:
            songs.append(songy)
        all_comments=comment.objects.all()    
        comments=[]
        for comm in all_comments:
            for sng in songs:
                if comm.song==sng:
                    comments.append(comm)

        context ={
            'playlist':Playlist,
            'songs':songs,
            'comments':comments,
        }
        return render (request, 'playlist_details_guest.html', context)

@login_required(login_url="/login")
def create_playlist(request, tribe_id):
    tribe=get_object_or_404(Tribe, pk=tribe_id)
    if request.user == tribe.chieftain:
        if request.method== 'POST':
            form=CreatePlaylistForm(request.POST)
            
            if form.is_valid():
                NewPlaylist=form.save(commit=False)
                NewPlaylist.tribe = get_object_or_404(Tribe, pk=tribe_id)
                NewPlaylist.creator=request.user
                NewPlaylist=form.save()
                return HttpResponseRedirect(reverse('playlist_detail', args=[NewPlaylist.id,]))

        else:
            form=CreatePlaylistForm()
            context= {'form':form}
            return render(request, "create_playlist.html",context)
    else:
        messages.error(request, ('Only chieftain can create new Playlist.'))
        return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))

@login_required(login_url="/login")
def add_song(request, id):
    TTribe=get_object_or_404(playlist, pk=id).tribe
    users=Membership.objects.filter(Tribe=TTribe).values('User')
    user_list=[]
    for i in users:
        user_list.append(i['User'])
    user_list.append(TTribe.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            form=AddSongForm(request.POST)
            
            if form.is_valid():
                NewSong=form.save(commit=False)
                NewSong.Playlist = get_object_or_404(playlist, pk=id)
                NewSong.user_added=request.user
                NewSong=form.save()
                return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

        else:
            form=AddSongForm()
            context= {'form':form}
            return render(request, "add_song.html",context)
    else: 
        messages.info(request, ('You are not a member in this tribe, you cannot add songs! Join tribe to add songs'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))


@login_required(login_url="/login")
def delete_song(request, id, chief_id):
    
    obj = get_object_or_404(song, id = id)
    context ={}
    if request.user == obj.user_added or request.user.id == chief_id:
        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect("/") 

        return render(request, "delete_view.html", context)
    else:
        messages.info(request, ('Only chieftain and member who added the song can delete it :('))
        return HttpResponseRedirect('/')

@login_required(login_url="/login")
def playlist_edit (request, id):
    TTribe=get_object_or_404(playlist, pk=id).tribe
    if request.user == TTribe.chieftain:
        thisplaylist=get_object_or_404(playlist, id=id)
        if request.method == 'POST':
            
            playlist_form = CreatePlaylistForm(request.POST, instance=thisplaylist)
            if  playlist_form.is_valid():
                playlist_form.save()
                messages.success(request, ('Your playlist was successfully updated!'))
                return redirect('home')
            else:
                messages.error(request, ('Please correct the error below.'))
        else:
            playlist_form = CreatePlaylistForm(instance=thisplaylist)
            
        return render(request, 'create_playlist.html', {
            'form': playlist_form
        })
    else:
        messages.info(request, ('Only chieftain can edit playlist info! You can ask him in chat :)'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

@login_required(login_url="/login")
def delete_playlist(request, id):
    TTribe=get_object_or_404(playlist, pk=id).tribe
    if request.user == TTribe.chieftain: 
        context ={} 
        obj = get_object_or_404(playlist, id = id) 
    
        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect("/") 
    
        return render(request, "delete_view.html", context)
    else:
        messages.info(request, ('Only chieftain can delete playlists! You can ask him in chat :)'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))