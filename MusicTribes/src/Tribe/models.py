from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.
class Tribe(models.Model):
    tribe_id = models.AutoField(primary_key=True)
    genre=models.TextField(max_length=50)
    tribeName=models.TextField(max_length=50)
    users=models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="usersRel", through='Membership')
    image=models.ImageField(null=True, blank=True, upload_to="TribePictures/", default="TribePictures/default123.png")
    chieftain = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=True,
        related_name="chiefRel",
    )
    def __str__(self):
            return self.tribeName
    

class Membership(models.Model):
    User = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, )
    Tribe = models.ForeignKey(Tribe,on_delete=models.CASCADE,)
    date_joined = models.DateTimeField(default=timezone.now )
    def __str__(self):
        return self.Tribe.tribeName


class Message(models.Model):
    mess_from=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,blank=True,)
    mess_to=models.ForeignKey(Tribe,on_delete=models.CASCADE,blank=True,)
    mess_content=models.TextField(max_length=200)
    time_sent=models.DateTimeField(default=timezone.now )
    def __str__(self):
        return self.mess_from.username