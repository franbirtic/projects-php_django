from django.contrib import admin

from .models import Tribe, Membership, Message

admin.site.register(Tribe)
admin.site.register(Membership)
admin.site.register(Message)