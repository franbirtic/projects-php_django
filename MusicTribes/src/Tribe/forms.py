from django.forms import ModelForm
from .models import Tribe, Message

class CreateTribeForm(ModelForm):
    class Meta:
        model=Tribe
        fields = ['tribeName', 'genre', 'image']

class CreateMessageForm(ModelForm):
    class Meta:
        model = Message
        fields=['mess_content',]